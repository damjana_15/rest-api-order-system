Start server with the command `./gradlew bootRun`

REST API endpoints:

Method | Endpoint | Body | Return | Note
--- | --- | --- | --- | ---
POST | /customers/new | {"firstName": "*firstName*", "lastName": "*lastName*" } | *customerNumber* | Adds new customer to database
GET | /customers/*number* | | {"number" : *number*, "firstName": "*firstName*", "lastName": "*lastName*"} | Returns customer with given number
POST | /products/new | {"price": *price*, "description": *description*} | *sku* | Adds new product to database
GET | /products/*sku* | | {"sku": *sku*, "price": *price*, "description": *description*} | Returns product with given number
POST | /orders/new/*customerNumber* | | *orderNumber* | Adds new order to database for this customer
GET | /orders/*number*/customer | | *customerNumber* | Returns customer for this order number
GET | /orders/*number*/lines | | [{"sku": *sku*, "quantity": *quantity*}, ...] | Returns array of order lines for this order
PUT | /orders/*number*/lines | {"sku": *sku*, "quantity": *quantity*} | | Increase quantity of the product in this order
DELETE | /orders/*number*/lines | {"sku": *sku*, "quantity": *quantity*} | | Decrease quantity of the product in this order
Get | /customers/arrayOfCustomers | | [{"firstName": *firstName*, "lastName": *lastName*},...] | Retruns array of all customers in the database
PUT | /customer/*number* | {"fistName": *firstName*, "lastName": *lastNAme*} | | Changes the first and last name of a customer with given number
GET | /customer/firstName/lastName/customerNumber | | {customerNumber} | Returns customer number given the first and last name of the customer.
GET | /orders/customerNumber/number | | [{"orderNumber"}, ...] | Returns all customer's order numbers given customer's number
GET | /orders/*customerNumber*/lines | | [{"sku": *sku*, "quantity": *quantity*}, ...] | Returns all products given cutomer's numeber
GET | /orders/number | | {"number": *number*, "firstName": "*firstName*", "lastName": "*lastName*", "sku": *sku*, "price": *price*, "description": *description*, "quantity": *quantity*} | Return an order form with all information

API number 21 is the API I had to design for the advanced Add-on. This API returns an array with all products that a customer has purched, given the Cusomer's number.It uses the order class and the lines database tables. As we had APIs that searched based on order number this will be based on the customer, doesn't matter what in what order these products are. 