package ordersystem;
/*
	Copyright (C) 2018 Karl R. Wurst
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import com.fasterxml.jackson.annotation.JsonAnyGetter;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for REST API endpoints for Customers
 * 
 * @author Karl R. Wurst
 * @version Fall 2018
 */
@RestController
public class CustomerController {

	private static final AtomicLong counter = Database.getCustomerCounter();
    private static Map<Long, Customer> customerDb = Database.getCustomerDb();
  
    /**
     * Create a new customer in the database
     * @param customer customer with first and last names
     * @return the customer number
     */
    @CrossOrigin() // to allow CORS requests when running as a local server
    @PostMapping("/customers/new")
    public ResponseEntity<Long> addNewCustomer(@RequestBody Customer customer) {
    	customer.setNumber(counter.incrementAndGet());
    	customerDb.put(customer.getNumber(), customer);
    	return new ResponseEntity<>(customer.getNumber(), HttpStatus.CREATED);
    }
    
    /**
     * Get a customer from the database
     * @param number the customer number
     * @return the customer if in the database, not found if not
     */
    @GetMapping("/customers/{number}")
    public ResponseEntity<Object> getCustomerByNumber(@PathVariable long number) {
    	if (customerDb.containsKey(number)) {
            return new ResponseEntity<>(customerDb.get(number), HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
    	}
    } 

    /**
     * Get all customers in the database
     * @param customer
     * @return all customers in the database  
     */
    @GetMapping("/customers/arrayOfCustomers")
    public ResponseEntity<Object> getArrayOfCustomers(@PathVariable List<Customer> arrayOfCustomers){
        if(arrayOfCustomers.isEmpty()){
            return new ResponseEntity<>("No customers in the database", HttpStatus.NOT_FOUND);
        } else {
            for (int i = 0; i < arrayOfCustomers.size(); i++){
                return new ResponseEntity<>(arrayOfCustomers.get(i), HttpStatus.OK);
            }
        }
    }

    /**
     * Change the first and last name of a customer
     * @param number the customer number
     * @return the new first and last name of the customer 
     */
    @GetMapping("/customers/{number}")
    public ResponseEntity<String> changeCustomerfandl(@PathVariable long number, @RequestBody String firstName, @RequestBody String lastName, @RequestBody Customer customer) {
        if (customerDb.containsKey(number)) {
            customerDb.get(number).setFirstName(firstName);
            customerDb.get(number).setLastName(lastName);
            customerDb.put(customer.getNumber(), customer);
            return new ResponseEntity<>("Customer first and last name sucesfully updated", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
        }
    }

     /**
     * Get the customer's number given the first and last name of a customer
     * @param firstName first name of the customer
     * @param lastName last name of the customer
     * @return the customer's number
     */
    @GetMapping("/customers/firstName/lastName/customerNumber")
    public ResponseEntity<Long> getCustomerNumber(@PathVariable long number, @PathVariable Customer firstName, @PathVariable Customer lastName, @PathVariable Customer customer, @PathVariable Customer customerNumber){
        if (customerDb.containsValue(firstName) & customerDb.containsValue(lastName)){
            return new ResponseEntity<>(customer.getNumber(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
       
}